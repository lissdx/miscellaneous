-module(erl2_primitive_server).
-export([start/2]).

%%
%% Simple Generic server example
%% 
%% Used Joe Armstrong - Programming Erlang Software for a Concurrent World 2 edition (Pragmatic programmers) - 2013
%% Francesco Cesarini and Stephen Vinoski Designing_for_Scalability_with_Erlang_OTP
%% Hebert F ( 2013 ) Learn You Some Erlang for Great Good 
%%
%% Start and register process with Module's name

start( Module, Args ) ->
    register( Module, spawn( ?MODULE, init, [ Module, Args ])).

init( Module, Args ) ->
    State = Module:init( Args ),
    loop( Module, State ).

rpc( Name, Request ) ->
    Name ! { self(), Request },
    receive 
	{ Name,  Response } ->
	    Response
    end.

loop( Module, State ) ->
    receive
	{ From, Request } = Msg -> 
	    { Response, NewState } = Module:handle( Msg, State ),
	    From ! { Module, Response },
	    loop( Module, NewState )
    end.
