-module(erl2c).
-export([compile/1]).

-define( MODS, "./" ).
-define( EXT, ".erl" ).
-define( DELEXT, ".beam").
-define( OUTDIR, "ebin" ).
-define( SRCDIR, "src" ).
-define( SKIPMODULE, ["erl2c"] ).


compile( PrjPath ) ->
    Module2CompileList = get_module_list( PrjPath ++ "/" ++ ?SRCDIR ),
    io:format("Module2CompileList : ~p~n",[ Module2CompileList ]),
    clean(  PrjPath, Module2CompileList ),
    compile( PrjPath, Module2CompileList ).
    
clean( PrjPath, [ Head | Tail ] ) -> 
    FileToDelete = PrjPath ++ "/" ++ ?OUTDIR ++ "/" ++ Head ++ ?DELEXT,
    io:format("File delete : ~p~n",[ FileToDelete ]),
    case file:delete( FileToDelete ) of
	{error, Reason} -> io:format("Can't delete file ~p. - ~p~n",[ FileToDelete, Reason ]);
	_ -> io:format("File ~p deleted ~n",[ FileToDelete ])
    end,
    clean( PrjPath,  Tail );
clean( _, []) -> 
    ok.

compile( PrjPath, [ Head | Tail ]) ->
    FileToCompile = PrjPath ++ "/" ++ ?SRCDIR ++ "/" ++ Head ++ ?EXT,
    OutDirPath = PrjPath ++ "/" ++ ?OUTDIR ++ "/",
    io:format("File compile : ~p~n",[ FileToCompile ]),
    case compile:file( FileToCompile, [{ outdir, OutDirPath }, export_all, debug_info ] ) of
	{ok, _} -> io:format( "Compiled file ~p.~n", [ FileToCompile ]);
	Error -> io:format( "Cant compile file ~p. -  ~p~n", [ FileToCompile, Error ])
    end,
    compile( PrjPath,  Tail );
compile( _, [] ) -> ok.

get_module_list( Path )->
    %% Filter files.
    %% Don't compile and delte modules from ?SKIPMODULE list
    {ok, FileList } = file:list_dir(Path),
    [ filename:basename( FileName, ?EXT ) || FileName <- FileList, not ( lists:member( filename:basename( FileName, ?EXT ), ?SKIPMODULE )) and  (filename:extension( FileName ) =:= ?EXT )].


    
%%%%% module_list(Path) ->
%%%%%     SameExt = fun(File) -> get_ext(File) =:= ?EXT end,
%%%%%     {ok, Files} = file:list_dir(Path),
%%%%%     lists:filter(SameExt, Files).
