%% 
%% Use erl2_primitive_server
%% 

-module(frequency_server).
-export([start/1, handle/2]).
-define( GEN_PRIMITIVE_SERVER, erl2_primitive_server ).


%% Start Primitive Generic server
start( Args ) ->
    ?GEN_PRIMITIVE_SERVER:start( ?MODULE, Args ).

%% Init data ( State )
init( Args ) ->
    { Args, [] }.
    
rpc( Request ) ->
    ?GEN_PRIMITIVE_SERVER:rpc( ?MODULE, Request ).

%% Handle requests on server side
handle( { _Pid, allocate }, State = {[], UsedFrequencyList } ) -> 
    { { allocate_error, free_frequency_empty }, State };
handle( { Pid, allocate }, {[ Head | Tail ], UsedFrequencyList }) -> 
    { {ok, { Pid, Head } }, { Tail, [  { Pid, Head } | UsedFrequencyList ] }};
handle( { _Pid, deallocate }, State = { FreeFrequencyList, [] } ) -> 
    { { deallocate_error, allocated_frequency_empty }, State };
handle( { Pid, deallocate }, State = { FreeFrequencyList, UsedFrequencyList } ) -> 
    case lists:keytake( Pid, 1, UsedFrequencyList ) of
	false -> { { deallocate_error, allocated_frequency_ellement_not_found }, State };
	{ value, { Pid, FrequencyElement }, NewList } ->  { ok, { [ FrequencyElement | FreeFrequencyList ], NewList }}
    end.



