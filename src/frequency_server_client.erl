%% 
%% Client of erl2_primitive_server
%% 

-module(frequency_server_client).
-export([allocate/0, deallocate/0]).
-define( FREQUENCY_SERVER_NAME, frequency_server ).

start() ->
    spawn( ?MODULE, init, []).

init() ->
    {ok, { Pid, Head } }= allocate(),
    io:format("Allocated State ~p.~n",[{ self(), Head}]),
    timer:sleep(5000), 
    deallocate(),
    io:format("Deallocated State ~p.~n",[{ self() }]). 

allocate() ->
    ?FREQUENCY_SERVER_NAME:rpc( allocate ).
deallocate() ->
    ?FREQUENCY_SERVER_NAME:rpc( deallocate ).







