%%% Created :  8 Jul 2014 by yuri lisnovsky

-module(typical_process_loop).
-export([start/1]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Generic interface 

start( Args ) ->
    spawn( ?MODULE, init, [Args] ).

init( Args ) ->
    State = initialize_state( Args ),
    loop( State ).

handle( Msg, State ) ->
    msg_handle( Msg, State ).

terminate( State ) ->
    clean_up( State ).

loop( State ) ->
    receive
	{ handle, Msg } ->
	    NewState = handle( Msg, State ),
	    loop( State );
	stop -> terminate( State )
    end.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Your implementation

initialize_state( Args ) ->
    Args.
clean_up( State ) ->
    ok.
msg_handle( _Msg, State ) ->
    State.
